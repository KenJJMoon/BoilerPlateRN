import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React from 'react';
import { StatusBar } from 'react-native';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import styled, { ThemeProvider } from 'styled-components';

import { DarkTheme, LightTheme } from '../compos/theme';
import InitialScreen from '../screens/InitialScreen';
import MainBottomTabNavigator from '../screens/MainTabNavigator';

// import useUserStore from '../../hooks/store/useUserStore';
// import * as AnalyticsService from '../../services/analyticsService';
// import {navigationRef} from '../../services/navigationService';
// import {ScreenProps} from '../../types';
// import ServiceNotification from '../scenes/homeStack/Service.screen';
// import {AppRoute} from './AppRoutes';
// import HomeNavigator from './Home.navigator';
// import OnboardingNavigator from './Onboarding.navigator';
export type AppNavigatorParams = {
  MAIN: undefined;
  INIT: undefined;
};

const AppStack = createStackNavigator<AppNavigatorParams>();

type StackNavigatorProps = React.ComponentProps<typeof AppStack.Navigator>;

interface AppNavigatorProps {
  // screenProps: ScreenProps;
}

function AppNavigator(
  props: AppNavigatorProps & Partial<StackNavigatorProps>,
): React.ReactElement {
  const scheme = useColorScheme();

  // console.log(' theme : ', DarkTheme, DefaultTheme);
  return (
    <AppearanceProvider>
      <NavigationContainer theme={scheme === 'dark' ? DarkTheme : LightTheme}>
        <ThemeProvider theme={scheme === 'dark' ? DarkTheme : LightTheme}>
          <StatusBar backgroundColor="#3DE" />
          <AppStack.Navigator {...props} headerMode="none">
            <AppStack.Screen name="INIT" component={InitialScreen} />
            <AppStack.Screen
              name="MAIN"
              component={MainBottomTabNavigator}
              options={{
                headerShown: false,
                // stackPresentation: 'transparentModal',
                // animationTypeForReplace: 'pop',
                ...TransitionPresets.ModalSlideFromBottomIOS,
              }}
            />
          </AppStack.Navigator>
        </ThemeProvider>
      </NavigationContainer>
    </AppearanceProvider>
  );
}

export default AppNavigator;
