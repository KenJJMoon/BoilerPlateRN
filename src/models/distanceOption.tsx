export const distanceOption = [
  {
    dist: '300m',
    walk: '도보 5분',
    bike: '자전거 2분',
    car: '',
  },
  {
    dist: '1km',
    walk: '도보 20분',
    bike: '자전거 5분',
    car: '자동차 1분',
  },
  {
    dist: '5km',
    walk: '도보 1시간 40분',
    bike: '자전거 20분',
    car: '자동차 12분',
  },
  {
    dist: '10km',
    walk: '도보 어려움',
    bike: '자전거 40분',
    car: '자동차 20분',
  },
];
