import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { createContext, Dispatch } from 'react';

/**
 * Company Model
 */
export type CompanyMdl = {
  id: string;
  name: string;
  img: { // image link
    icon: string;
    main: string;
  },
  bizNum: string;
  phone: string;
  cxPhone: string;
};

export type CompanyStt = Array<CompanyMdl>;

export type CompanyAct =
| { type: 'TOUCHED'; param: string }
| { type: 'OTHER'; id: string };

export type CompanyDispatch = Dispatch<CompanyAct>;

/**  User State */
export type UserState = {
  id: string;
  email: string;
  password: string;
  passwordConfirm: string;
  fbUser: FirebaseAuthTypes.User | null;
};

export type UserAct =
| { type: 'SET_EMAIL_PW'; param: { email?: string; password?: string; passwordConfirm?: string; }}
// | { type: 'LOGIN'; }
// | { type: 'REGIST'; }
| { type: 'OTHER'; id: string };

export type UserDispatch = Dispatch<UserAct>;

/**  Auth State */
export type AuthState = {
  id: string;
  email: string;
  password: string;
  passwordConfirm: string;
  fbUser: FirebaseAuthTypes.User | null;
};

export type AuthAct =
| { type: 'SET_EMAIL_PW'; param: { email?: string; password?: string; passwordConfirm?: string; }}
// | { type: 'LOGIN'; }
// | { type: 'REGIST'; }
| { type: 'OTHER'; id: string };

export type AuthDispatch = Dispatch<AuthAct>;

export type DesignerWork = {
  id: number;
  name: string;
  address: string;
  heart: number;
  message: string;
  workImg: any;
  profileImg: any;
  like?: boolean;
}
