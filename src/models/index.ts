import { atom, useRecoilState } from 'recoil';
// Recoil State Management
export const filterState = atom({
  key: 'main/Filter',
  default: 'area',
});

export const likeState = atom({
  key: 'array/Likes',
  default: [
    { id: 0, heart: 158, like: true },
    { id: 1, heart: 353, like: false },
    { id: 2, heart: 567, like: false },
    { id: 3, heart: 38, like: false },
    { id: 4, heart: 11, like: false },
    { id: 5, heart: 15, like: false },
    { id: 6, heart: 58, like: false },
    { id: 7, heart: 8, like: false },
  ],
});

export * from './typeModel';
export * from './company/comState';
export * from './company/comActions';
export * from './company/comProvider';
export * from './auth/authActions';
export * from './auth/authProvider';
export * from './auth/authState';
