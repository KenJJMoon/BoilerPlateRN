
import axios, { AxiosError } from 'axios';
import { Alert } from 'react-native';

export const apiAgent = axios.create({
  baseURL: 'https://us-central1-hardpillar-663ad.cloudfunctions.net',
  // https://us-central1-hardpillar-663ad.cloudfunctions.net/HelloGet
  withCredentials: false,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});
