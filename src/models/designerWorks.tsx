import { DesignerWork } from './typeModel';

const work01 = require('../images/work01.png');
const work02 = require('../images/work02.png');
const work03 = require('../images/work03.png');
const work04 = require('../images/work04.png');

const profile01 = require('../images/profile01.png');
const profile02 = require('../images/profile02.png');
const profile03 = require('../images/profile03.png');
const profile04 = require('../images/profile04.png');

export const designerWorks: Array<DesignerWork> = [
  {
    id: 0,
    workImg: work01,
    profileImg: profile01,
    address: '서울시 강남구 서초동',
    name: '희햄 디자이너',
    heart: 138,
    message: '안녕하세요. 항상 감동을 선사하는 희햄입니다! 아름다움을 선물해 드릴게요',
    like: true,
  },
  {
    id: 1,
    workImg: work02,
    profileImg: profile02,
    address: '서울시 서대문구 서대동',
    name: '김다혜 디자이너',
    heart: 68,
    message: '그대의 아름다움에 매력을 몽땅 넣어드릴께요',
  },
  {
    id: 2,
    workImg: work03,
    profileImg: profile03,
    address: '서울시 성북구 성북동',
    name: '심건우 디자이너',
    heart: 520,
    message: '젊은 감각의 트렌디한 진짜 디자이너 건우쌤입니다!',
  },
  {
    id: 3,
    workImg: work04,
    profileImg: profile04,
    address: '서울시 강남구 강남동',
    name: '지니 디자이너',
    heart: 145,
    message: '안녕하세요. 지니 디자이너입니다. 화요일 11시 염색모델 구합니다!',
  },
  {
    id: 4,
    workImg: work01,
    profileImg: profile01,
    address: '서울시 강남구 서초동',
    name: '희햄 디자이너a',
    heart: 138,
    message: '안녕하세요. 항상 감동을 선사하는 희햄입니다! 아름다움을 선물해 드릴게요',
  },
  {
    id: 5,
    workImg: work02,
    profileImg: profile02,
    address: '서울시 서대문구 서대동',
    name: '김다혜 디자이너a',
    heart: 68,
    message: '그대의 아름다움에 매력을 몽땅 넣어드릴께요',
  },
  {
    id: 6,
    workImg: work03,
    profileImg: profile03,
    address: '서울시 성북구 성북동',
    name: '심건우 디자이너a',
    heart: 520,
    message: '젊은 감각의 트렌디한 진짜 디자이너 건우쌤입니다!',
  },
  {
    id: 7,
    workImg: work04,
    profileImg: profile04,
    address: '서울시 강남구 강남동',
    name: '지니 디자이너a',
    heart: 145,
    message: '안녕하세요. 지니 디자이너입니다. 화요일 11시 염색모델 구합니다!',
  },
];
