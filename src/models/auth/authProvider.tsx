import React, { useReducer } from 'react';

import { AuthDispatchContext, authReducer } from './authActions';
import { AuthContext } from './authState';

const initUser = {
  id: '',
  email: '',
  password: '',
  passwordConfirm: '',
  fbUser: null,
};

export function AuthProvider({ children }: { children: React.ReactNode }): React.ReactElement {
  const [company, dispatch] = useReducer(authReducer, initUser);

  return (
    <AuthDispatchContext.Provider value={dispatch}>
      <AuthContext.Provider value={company}>
        {children}
      </AuthContext.Provider>
    </AuthDispatchContext.Provider>
  );
}
