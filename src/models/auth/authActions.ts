
import { createContext, Dispatch, useContext } from 'react';

import { AuthAct, AuthDispatch, AuthState } from '../typeModel';

export const AuthDispatchContext = createContext<AuthDispatch | undefined>(undefined);

export function authReducer(state: AuthState, action: AuthAct): AuthState {
  switch (action.type) {
  case 'SET_EMAIL_PW':
    console.log(' Touched: ', action.param);

    return { ...state, ...action.param };
  default:
    throw new Error('Unhandled Company action');
  }
}

export function useAuthDispatch(): Dispatch<AuthAct> {
  const dispatch = useContext(AuthDispatchContext);
  if (!dispatch) throw new Error('Provider not found');
  return dispatch;
}
