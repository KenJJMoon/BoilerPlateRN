import React, { Context, createContext, Dispatch, useContext, useReducer } from 'react';

import { AuthState } from '../typeModel';

export const AuthContext = createContext<AuthState | undefined>(undefined);

export function useAuthState(): AuthState {
  const state = useContext(AuthContext);
  if (!state) throw new Error('Auth Provider not found');
  return state as AuthState;
}
