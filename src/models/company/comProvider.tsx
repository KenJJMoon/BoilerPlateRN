import React, { useReducer } from 'react';

import { CompanyDispatchContext, companyReducer } from './comActions';
import { CompanyContext } from './comState';

const initCompanies = [
  {
    id: 'initvalue',
    name: '(주)평평 메인티넌스',
    img: {
      icon: 'some link',
      main: 'other link',
    },
    bizNum: '123-333-234444',
    phone: '123-3030-4444',
    cxPhone: '070-333-4433',
  },
];

export function CompanyProvider({ children }: { children: React.ReactNode }): React.ReactElement {
  const [company, dispatch] = useReducer(companyReducer, initCompanies);

  return (
    <CompanyDispatchContext.Provider value={dispatch}>
      <CompanyContext.Provider value={company}>
        {children}
      </CompanyContext.Provider>
    </CompanyDispatchContext.Provider>
  );
}
