import React, { Context, createContext, Dispatch, useContext, useReducer } from 'react';

import { CompanyStt } from '../typeModel';

export const CompanyContext = createContext<CompanyStt | undefined>(undefined);

export function useCompanyState(): CompanyStt {
  const state = useContext(CompanyContext);
  if (!state) throw new Error('Company Provider not found');
  return state as CompanyStt;
}
