
import { createContext, Dispatch, useContext } from 'react';

import { CompanyAct, CompanyDispatch, CompanyStt } from '../typeModel';

export const CompanyDispatchContext = createContext<CompanyDispatch | undefined>(undefined);

export function companyReducer(state: CompanyStt, action: CompanyAct): CompanyStt {
  switch (action.type) {
  case 'TOUCHED':
    console.log(' Touched: ', action.param);
    return state;
  default:
    throw new Error('Unhandled Company action');
  }
}

export function useCompanyDispatch(): Dispatch<CompanyAct> {
  const dispatch = useContext(CompanyDispatchContext);
  if (!dispatch) throw new Error('TodosProvider not found');
  return dispatch;
}
