import _ from 'lodash';
import numeral from 'numeral';
import { Dimensions, Platform } from 'react-native';

export const { width, height } = Dimensions.get('window');
// W / H : 640 X 360
const ratio = (16 * (width / height));
const scrR = (ratio < 9 ? width / 9 : height / 16) / (360 / 9);
const sr = scrR;
console.log(`\n   Screen: ${width}x${height} screenRatio : ${scrR}`);
const isIPhoneX = Math.floor(height / width * 1000) === 2165;
export { scrR, sr, isIPhoneX };
export const statusHeight = Platform.OS === 'ios' ? 20 : 0;
export const navH = Platform.OS === 'ios' ? (76 * scrR) : (56 * scrR);
export const wid328 = 328 * sr;

// export const topDist = (Platform.OS === 'ios') ? 25 * sr : 15 * sr;
export const topDist = isIPhoneX ? 40 * sr : ((Platform.OS === 'ios') ? 25 * sr : 15 * sr);

export function getYYMMDD(now) {
  const shortYear = `${now.getFullYear()}`.substring(2, 4);
  const mon = `${now.getMonth() + 1}`;
  const day = `${now.getDate()}`;
  const monthWith0 = mon.length < 2 ? `0${mon}` : mon;
  const dayWith0 = day.length < 2 ? `0${day}` : day;
  return `${shortYear}/${monthWith0}/${dayWith0}`;
}

export function getString(str, nilStr) {
  return _.isNil(str) ? nilStr : `${str}`;
}

export function isValidStr(str, len) {
  return !_.isNil(str) && len < str.length;
}

export function filterCountry(country, arrKy) {
  let rslt = false;
  if (_.isNil(country)) return false;
  arrKy.map((k) => rslt = rslt || country.includes(k));
  return rslt;
}

export function randomkeyExtractor(it, index) {
  // _keyExtractor = (item, index) => `${index} ${item.modify_time}`;
  return `${index} ${Math.random()}`;
}

export function anyNil(arr) {
  for (let i = 0; i < arr.length; i += 1) {
    if (_.isNil(arr[i])) return true;
  }
  return false; // at least one NIL object
}

export function allNil(arr) {
  if (arr.length === 0) return false;
  const nilObjs = arr.filter((o) => _.isNil(o));
  return nilObjs.length === arr.length; // at least one NIL object
}

export function validArray(arrOfArr, minNum) {
  for (let i = 0; i < arrOfArr.length; i = 1 + i) {
    if (!(arrOfArr[i] && _.isArray(arrOfArr[i]) &&
      arrOfArr[i].length >= minNum)) return false;
  }
  return true;
}

export function getFitSize(w, h, outWid, outHei) {
  const nega = [w, h, outWid, outHei].filter((n) => n <= 0).length;
  if (nega > 0) return { wid: outWid, hei: outHei };
  const inR = h / w; const outR = outHei / outWid;
  // console.log('in ratio: ', inR, ' out ratio: ', outR);
  if (inR < outR) {
    return { wid: outWid, hei: outWid * inR };
  }
  return { wid: outHei / inR, hei: outHei };
}

export function dateDotSeparate(now) {
  const theYear = now.getFullYear();
  const mon = `${now.getMonth() + 1}`;
  const day = `${now.getDate()}`;
  // const monthWith0 = mon.length < 2 ? `0${mon}` : mon;
  // const dayWith0 = day.length < 2 ? `0${day}` : day;
  return `${theYear}. ${mon}. ${day}`; // 1989. 12. 5
}

export function korDateTime(tstampStr) {
  const ts = new Date(tstampStr);
  const hr = ts.getHours();
  const hrStr = hr > 12 ? `오후 ${hr - 12}` : `오전 ${hr}`;
  return `${ts.getMonth() + 1}월 ${ts.getDate()}일 ${hrStr}시 ${ts.getMinutes()}분`;
}

export function monthDotDate(now) {
  const mon = `${now.getMonth() + 1}`;
  const day = `${now.getDate()}`;
  // const monthWith0 = mon.length < 2 ? `0${mon}` : mon;
  // const dayWith0 = day.length < 2 ? `0${day}` : day;
  return `${mon}. ${day}`; // 12. 5
}

export function fixed(str, ix) {
  return parseFloat(str).toFixed(ix);
}

export function subtractOne(val) {
  const rv = val - 1;
  return rv < 0 ? 0 : rv;
}

export function getKoreanDate(now) {
  const yy = `${now.getFullYear()}년`;
  const mon = `${now.getMonth() + 1}월`;
  const day = `${now.getDate()}일`;
  const hour = `${now.getHours()}시 ${now.getMinutes()}분`;
  return `${yy} ${mon} ${day} ${hour}`;
}

export function getNumbers(txt) {
  return txt.replace(/[^0-9]/g, '');
}

export function formatPhoneNumber(txt) {
  const nmbr = getNumbers(txt);
  let nm = txt;
  nm = nmbr.length > 3 ? `${nmbr.substring(0, 3)}-${nmbr.substring(3)}` : nm;
  nm = nmbr.length > 7
    ? `${nmbr.substring(0, 3)}-${nmbr.substring(3, 6)}-${nmbr.substring(6)}`
    : nm;
  nm = nmbr.length >= 11
    ? `${nmbr.substring(0, 3)}-${nmbr.substring(3, 7)}-${nmbr.substring(7, 11)}`
    : nm;
  return nm;
}

export function checkPassword(value, len = 8) {
  // const mediumRegex = /^(?=.{6,20})((?=.*[A-Z])|(?=.*[a-z])|(?=.*[0-9])|(?=.*[~!@\#$%<>^&*()-=+_])).*$/;
  const mediumRegex = /^(?=.{6,20})((?=.*[A-Z])|(?=.*[a-z])|(?=.*[0-9])|(?=.*[~!@#$%<>^&*()-=+_])).*$/;
  // console.log(` PW chk : ${value}`);
  if (!mediumRegex.test(value)) {
    return false;
  }
  if (value.length < len || value.length > 20) { // console.log(' Length Error');
    return false;
  }
  return true;
}

export function addComma(num) {
  const rnum = Number(num);
  if (_.isNaN(rnum)) {
    return '-1';
  }
  return numeral(rnum).format('0,0');
}

export function removeComma(num) {
  // const rnum = Number(num.replace(/\,/g, ''));
  const rnum = Number(num.replace(/\D+/g, ''));
  return rnum === 0 ? '' : rnum;
}

// Number.prototype.formatWithComma = function () {
//   if (this === 0) {
//     return 0;
//   }
//   const reg = /(^[+-]?\d+)(\d{3})/;
//   let n = (`${this}`);
//   while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');
//   return n;
// };
