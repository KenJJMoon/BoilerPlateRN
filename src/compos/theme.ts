
export const LightTheme = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: '#FCFCFC',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    titleMid: '#234',
    border: '#99B',
    error: '#Fa3a4a',
  },
};

export const DarkTheme = {
  dark: true,
  colors: {
    primary: 'rgb(10, 132, 255)',
    background: '#333',
    card: 'rgb(18, 18, 18)',
    text: 'rgb(229, 229, 231)',
    titleMid: '#FaFaFf',
    border: '#AaAaBf',
    error: '#FfBbBa',
  },
};

export type ThemeType = typeof LightTheme;
