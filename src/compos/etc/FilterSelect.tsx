import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInputProps,
  TouchableOpacity,
  View,
} from 'react-native';
import { useRecoilState } from 'recoil';
import styled from 'styled-components/native';

import { filterState } from '../../models';
import { HeaderStyle, Input, SubContent, Wrapper } from '../uiCommon';

const Container = styled.TouchableOpacity
<{
  isActive?: boolean;
}>`
  padding: 7px 17px 0px 10px;
  margin: 7px;
  height: 30px;
  background: ${(p) => p.isActive ? '#5f40e611' : '#0000'};
  align-items: center;
  border-radius: 15px;
  border-color: ${(p) => p.isActive ? '#5f40e6' : '#646464'};
  border-width: 1px;
`;

const TheInput = styled(Input)`
  margin: 10px -3px;
  padding: 5px;
  border-color: ${(p) => p.theme.colors.border};
  border-width: 1px;
`;

const Title = styled(HeaderStyle)`
  color: #292929;
  font-size: 13px;
`;

const ErrorText = styled(SubContent)`
  color: ${(p) => p.theme.colors.error};
`;

interface FilterProp extends TextInputProps {
  title: string;
  targetString: string;
}

export function FilterSelect(props: FilterProp) : React.ReactElement {
  const { title, targetString } = props;
  const [filterStr, setFilterStr] = useRecoilState(filterState);
  const isActive = targetString === filterStr;
  const theTitle = targetString === 'area' && filterStr !== 'area'
    ? (isActive ? title : '압구정/청담')
    : title;
  return (
    <Container
      isActive={isActive}
      onPress={() => {
        if (!filterStr || !isActive) {
          setFilterStr(targetString);
        } else {
          setFilterStr('');
        }
      }}
    >
      <Title>{theTitle}</Title>

    </Container>
  );
}
