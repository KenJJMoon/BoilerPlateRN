import React, { useState } from 'react';
import styled from 'styled-components/native';

const Container = styled.TouchableOpacity`
  flex-direction: row;
  padding: 0px 0px;
  margin: 7px;
  height: 40px;
  width: 252px;
  background: #fff;
  align-items: center;
  border-radius: 20px;
  border-color: #646464;
  border-width: 1px;
  
`;

const Button = styled.TouchableOpacity<{
  isActive?: boolean;
}>`
  flex: 1;
  align-self: stretch;
  align-items: center;
  justify-content: center;
  background: #0000;
`;

const ButtonMid = styled(Button)`
  border-right-width: 1px;
  border-left-width: 1px;
  border-color: #29292999;

`;

const Title = styled.Text<{
  isActive?: boolean;
}>`
  color: ${(p) => p.isActive ? '#5f40e6' : '#292929'};
  font-size: 14px;
  font-weight: bold;
`;

export function Operations() : React.ReactElement {
  const [sel, setSelect] = useState(0);
  return (
    <Container>
      <Button
        onPress={() => setSelect(0)}
      >
        <Title isActive={sel === 0} >일반시술</Title>
      </Button>
      <ButtonMid
        onPress={() => setSelect(1)}
      >
        <Title isActive={sel === 1}>모델시술</Title>
      </ButtonMid>
      <Button
        onPress={() => setSelect(2)}
      >
        <Title isActive={sel === 2}>전체</Title>
      </Button>
    </Container>
  );
}
