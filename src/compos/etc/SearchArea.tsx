import React, { useEffect, useState } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInputProps,
  TouchableOpacity,
  View,
} from 'react-native';
import { useRecoilState } from 'recoil';
import styled from 'styled-components/native';

import { filterState } from '../../models';
import { distanceOption } from '../../models/distanceOption';
import { HeaderStyle, Input, SubContent, ViewRowWrapper, Wrapper } from '../uiCommon';

const lineHor = require('../../images/line_hor.png');
const lineVert = require('../../images/line_vert.png');
const imgWalk = require('../../images/directions_walk.png');
const imgBike = require('../../images/directions_bike.png');
const imgCar = require('../../images/directions_car.png');

const Container = styled.View`
  padding: 15px;
  height: 300px;
`;
const GrayButton = styled.TouchableOpacity`
  flex: 1;
  padding: 8px;
  margin: 20px 0px;
  border-color: #29292933;
  border-width: 1px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
`;
const DistButton = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Line = styled.View`
  width: 100%;
  border-color: #29292933;
  border-bottom-width: 1px;
  margin: 20px 5px;
`;
const AreaContainer = styled.View`
  flex-direction: row;
  padding: 3px 0px;
  margin: 8px 0px;
`;

const VertLineImg = styled.Image`
  width: 5px;
  height: 11px;
`;

const Title = styled(HeaderStyle)`
  color: #292929;
  font-size: 18px;
  font-weight: bold;
`;
const ButtonText = styled(HeaderStyle)`
  color: #29292999;
  font-size: 15px;
  font-weight: bold;
`;
const PlainText = styled.Text`
  color: #292929aa;
  font-size: 14px;
  line-height: 30px;
`;
const DistText = styled(ButtonText)`
  color: #292929;
  font-size: 16px;
  font-weight: 400;
`;

const Dot = styled.View`
  width: 20px;
  height: 20px;
  background: #5f40e6;
  border-radius: 10px;
`;

// Distance Guide
const GuideContainer = styled(ViewRowWrapper)`
  margin: 10px 20px;
  align-items: center;
`;
const ImgDirect = styled.Image`
  width: 27px;
  height: 27px;
  margin: 2px 5px;
`;

export function SearchArea() : React.ReactElement {
  const [distIdx, setDist] = useState(1);
  const dotDist = distIdx * 95 + 43;
  const { dist, walk, bike, car } = distanceOption[distIdx];
  return (
    <Container>
      <Title>찾으시는 지역을 설정해주세요</Title>
      <PlainText>원하시는 지역을 중심으로 디자이너를 탐색해드려요.</PlainText>

      <ViewRowWrapper>
        <GrayButton>
          <ButtonText>내 위치</ButtonText>
        </GrayButton>
        <View style={{ padding: 7 }} />
        <GrayButton>
          <ButtonText>지역 검색</ButtonText>
        </GrayButton>
      </ViewRowWrapper>

      <Line />

      <Title>탐색 범위를 설정해주세요</Title>
      <PlainText>범위 내의 디자이너를 탐색해드려요.</PlainText>

      <AreaContainer>
        <Image
          source={lineHor} resizeMode="contain"
          style={{ position: 'absolute', top: 8, left: 0, width: '100%' }}
        />
        <View style={{ flex: 0.5 }} />
        <VertLineImg source={lineVert} resizeMode="contain" />
        <View style={{ flex: 1 }} />
        <VertLineImg source={lineVert} resizeMode="contain" />
        <View style={{ flex: 1 }} />
        <VertLineImg source={lineVert} resizeMode="contain" />
        <View style={{ flex: 1 }} />
        <VertLineImg source={lineVert} resizeMode="contain" />

        <View style={{ position: 'absolute', top: 0, left: 0, flexDirection: 'row' }} >
          <View style={{ width: dotDist }} />
          <Dot />
        </View>
        <View style={{ flex: 0.5 }} />
      </AreaContainer>

      <AreaContainer>
        <DistButton onPress={() => setDist(0)} >
          <DistText>300m</DistText>
        </DistButton>
        <DistButton onPress={() => setDist(1)} >
          <DistText>1km</DistText>
        </DistButton>
        <DistButton onPress={() => setDist(2)} >
          <DistText>5km</DistText>
        </DistButton>
        <DistButton onPress={() => setDist(3)} >
          <DistText>10km</DistText>
        </DistButton>
      </AreaContainer>

      <GuideContainer>
        <ImgDirect source={imgWalk} />
        <DistText>{walk}</DistText>
      </GuideContainer>
      <GuideContainer>
        <ImgDirect source={imgBike} />
        <DistText>{bike}</DistText>
      </GuideContainer>
      {!!car && (
        <GuideContainer>
          <ImgDirect source={imgCar} />
          <DistText>{car}</DistText>
        </GuideContainer>
      )}

    </Container>
  );
}
