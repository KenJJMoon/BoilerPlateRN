import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInputProps,
  TouchableOpacity,
  View,
} from 'react-native';
import { useRecoilState } from 'recoil';
import styled from 'styled-components/native';

import { DesignerWork, likeState } from '../../models';
import { HeaderStyle } from '../uiCommon';
import { width } from '../uiUtils';

const imgHeartRed = require('../../images/heart_red.png');
const imgHeart = require('../../images/heart_black.png');

const Container = styled.View`
  flex: 1;
  flex-direction: row;
  height: 130px;
  padding: 0px 5px 20px 5px;
  margin: 10px 10px;
  align-items: center;
  justify-content: center;
  border-bottom-width: 1px;
  border-color: #29292933;
`;
const ViewContent = styled.View`
  flex: 1;
  justify-content: center;
`;
const ViewNameInfo = styled.View`
  flex: 1;
  justify-content: center;
`;
const HeartButton = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
`;

const ImageMain = styled.Image`
  width: 110px;
  height: 110px;
  margin: 0px 10px 0px 0px;
`;
const ImageProfile = styled.Image`
  margin: 5px;
  width: 50px;
  height: 50px;
  border-radius: 25px;
`;

const ImageHeart = styled.Image`
  width: 30px;
  height: 30px;
`;

const Title = styled(HeaderStyle)`
  color: #292929;
  font-size: 15px;
  font-weight: bold;
`;
const Address = styled.Text`
  color: #292929AA;
  font-size: 12px;
  margin: 8px 0px;
`;
const Message = styled.Text`
  color: #292929;
  font-size: 13px;
  margin: 8px 0px;
`;

interface FilterProp extends TextInputProps {
  data: DesignerWork;
  rData: any;
}

export function Profile(props: FilterProp) : React.ReactElement {
  const { id, workImg, profileImg, address, name, message } = props.data;
  const { rData } = props;
  const [likes, setLikes] = useRecoilState(likeState);
  const idx = likes.findIndex((l) => l.id === id);

  const toggleLike = () => {
    const newHeart = rData.like ? rData.heart - 1 : rData.heart + 1;
    const newState = { id, heart: newHeart, like: !rData.like };
    setLikes([
      ...likes.slice(0, idx),
      newState,
      ...likes.slice(idx + 1),
    ]);
  };

  return (
    <Container>
      <ImageMain source={workImg} resizeMode="contain" />
      <ViewContent style={{ flex: 1, flexDirection: 'column' }}>
        <ViewContent style={{ flexDirection: 'row' }}>
          <ImageProfile source={profileImg} resizeMode="stretch" />
          <ViewNameInfo style={{ flex: 1 }}>
            <Address>{address}</Address>
            <Title>{name}</Title>
          </ViewNameInfo>
          <HeartButton
            style={{ width: 30 }}
            onPress={toggleLike}
          >
            {rData.like
              ? <ImageHeart source={imgHeartRed} />
              : <ImageHeart source={imgHeart} />
            }
            <Address>{rData.heart}</Address>
          </HeartButton>
        </ViewContent>
        <Message>{message}</Message>
      </ViewContent>
    </Container>
  );
}
