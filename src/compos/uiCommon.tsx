import React, { ReactElement, ReactNode, useEffect, useState } from 'react';
import styled from 'styled-components/native';

import { sr } from './uiUtils';

/**  Font Size, Scroll View      with   theme background */
export const FontSize = {
  Title: sr * 18,
  Header: sr * 15,
  Content: sr * 12,
  Small: sr * 9,
  Input: sr * 14,
};

/**  Wrapper Component ::  Scroll View      with   theme background */
export const ScrollWrapper = styled.ScrollView`
  background: ${(p) => p.theme.colors.background};
`;
export const ViewWrapper = styled.View`
  background: ${(p) => p.theme.colors.background};
`;
export const ViewRowWrapper = styled(ViewWrapper)`
  flex-direction: row;
`;

export const Wrapper = styled(ViewWrapper)`
  margin: ${() => sr * 3}px ${() => sr * 10}px;
`;
// interface WrapProps { children: ReactNode; }
// export function Wrapper(props: WrapProps): ReactElement {
//   return (
//     <WrapView>
//       {props.children}
//     </WrapView>
//   );
// }

/**  Base components  */
export const HeaderStyle = styled.Text`
  font-size: ${() => FontSize.Header}px;
  font-weight: 600;
`;

export const SubContent = styled.Text`
  font-size: ${() => FontSize.Small}px;
  font-weight: 300;
`;

export const Input = styled.TextInput`
  font-size: ${() => FontSize.Input}px; 
`;
