import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInputProps,
  TouchableOpacity,
  View,
} from 'react-native';
import styled from 'styled-components/native';

import { HeaderStyle, Input, SubContent, Wrapper } from '../uiCommon';

const Container = styled(Wrapper)`
  padding: 7px 0px;
`;

const TheInput = styled(Input)`
  margin: 10px -3px;
  padding: 5px;
  border-radius: 5px;
  border-color: ${(p) => p.theme.colors.border};
  border-width: 1px;
`;

const Title = styled(HeaderStyle)`
  color: ${(p) => p.theme.colors.titleMid};
`;

const ErrorText = styled(SubContent)`
  color: ${(p) => p.theme.colors.error};
`;

interface AuthProp extends TextInputProps {
  title: string;
  errMsg: string;
}

export function AuthInputArea(props: AuthProp) : React.ReactElement {
  const { title, errMsg } = props;
  return (
    <Container>
      <Title>{title}</Title>
      <TheInput
        {...props}
        autoCapitalize="none"
        autoCompleteType="off"
        autoCorrect={false}
      />
      <ErrorText>{errMsg}</ErrorText>
    </Container>
  );
}
