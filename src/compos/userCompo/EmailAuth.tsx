import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import {
  KeyboardAvoidingView,
  Linking, ScrollView, Text,
} from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context';
import styled from 'styled-components/native';
import * as yup from 'yup';

import { useAuthDispatch, useAuthState } from '../../models';
import { Wrapper } from '../uiCommon';
import { AuthInputArea } from './AuthCompo';

const Container = styled(Wrapper)`
  padding: 7px 0px;
`;

const Button = styled.TouchableOpacity``;

function EmailAuth(): React.ReactElement {
  const insets = useSafeArea();

  const dispatch = useAuthDispatch();
  const authState = useAuthState();

  useEffect(() => {
    console.log('  email ', authState.email);
  }, [authState.email]);

  const schema = yup.object().shape({
    email: yup.string()
      .email('이메일 형식과 맞지 않아요.')
      .required(''),
    password: yup.string().min(6, '비밀번호는 6글자 이상입니다.'),
  });

  return (
    <Formik
      initialValues={{
        email: '', password: '',
      }}
      validationSchema={schema}
      onSubmit={(values, actions) => {
        actions.setSubmitting(true);
        dispatch({
          type: 'SET_EMAIL_PW',
          param: {
            email: values.email,
            password: values.password,
          },
        });
        // loginWithEmail({
        //   success: () => {},
        //   setSubmitting: actions.setSubmitting,
        // });
      }}
    >
      {({
        errors, isValid, values, handleChange, handleBlur,
        handleSubmit, touched, submitCount, dirty, isSubmitting,
      }) => {
        const fullValid = isValid && !!values.email && !!values.password;
        return (
          <Container style={{ flex: 1, marginTop: 38 }}>
            <AuthInputArea
              title="이메일"
              value={values.email}
              keyboardType="email-address"
              errMsg={errors.email || ''}
              placeholder="이메일을 입력해주세요"
              onChangeText={handleChange('email')}
            />

            <AuthInputArea
              title="비밀번호"
              value={values.password}
              errMsg={errors.password || ''}
              placeholder="비밀번호를 입력해주세요"
              onChangeText={handleChange('password')}
              secureTextEntry
            />

            <Button
              onPress={handleSubmit}
            >
              <Text>Submit</Text>
            </Button>

          </Container>
        );
      }}
    </Formik>
  );
}

export default EmailAuth;
