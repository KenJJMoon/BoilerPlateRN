/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import OneSignal from 'react-native-onesignal';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import styled from 'styled-components/native';

import { apiAgent } from '../models/api';

declare const global: {HermesInternal: null | {}};

const InitialScreen = () => {
  const NavButton = styled.TouchableOpacity``;

  const navigation = useNavigation();

  const onReceived = (noti: string) => { console.log(' Notification ', noti); };
  const onOpened = (openResult: any) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  const onIds = async (device: any) => {
    console.log('Device info: ', device, device.pushToken, device.userId);

    const { data } = await apiAgent.get('/HelloGet').catch((error) => {
      const statusCode = error.getError().response ? error.getError().response?.status : null;
      if (statusCode >= 300) {
        Alert.alert('Network Error', `Code : ${statusCode}`);
      } else {
        error.handleGlobally && error.handleGlobally();
      }
    });

    console.log(' Response :: ', data);
  };

  const myiOSPromptCallback = () => { console.log(' do something with permission '); };

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init('d9137328-6649-45a7-a6be-d9d2d5f0b10d',
      {
        kOSSettingsKeyAutoPrompt: false,
        kOSSettingsKeyInAppLaunchURL: false,
        kOSSettingsKeyInFocusDisplayOption: 2,
      });
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

    // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
    OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    setTimeout(() => {
      console.log(' Navigate to Main !!');
      navigation.navigate('MAIN');
    }, 2000);
    return () => {
      console.log('  Initial screen Clean up !!');
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  });

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Hard Pillar</Text>
              <NavButton onPress={() => navigation.navigate('MAIN')}>
                <Text>Navigate to Main</Text>
              </NavButton>
              <Text style={styles.sectionDescription}>Splash Screen</Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default InitialScreen;
