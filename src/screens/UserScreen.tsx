
import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { EmailAuth, ScrollWrapper, ViewWrapper } from '../compos';
import { AuthInputArea } from '../compos/userCompo/AuthCompo';
import { useCompanyState } from '../models';

// eslint-disable-next-line @typescript-eslint/ban-types
declare const global: {HermesInternal: null | {} };

const UserScreen = () => {
  const companies = useCompanyState();
  const [email, setEmail] = useState('');
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);

  // Handle user state changes
  function onAuthStateChanged(user: FirebaseAuthTypes.User | null) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  return (
    <>
      <SafeAreaView>
        <ScrollWrapper
          contentInsetAdjustmentBehavior="automatic"
        >
          <ViewWrapper >
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>User</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.tsx</Text> to change
                this screen and then come back to see your edits.
              </Text>
              {companies.map((com, idx) =>
                <Text key={idx}>
                  {`${com.name},  phone : ${com.phone}`}
                </Text>,
              )}

              <EmailAuth />

              <TouchableOpacity
                style={{ margin: 15 }}
                onPress={() => {
                  auth()
                    .createUserWithEmailAndPassword('mokadung@gmail.com', 'asdf23')
                    .then(() => {
                      console.log('User account created & signed in!');
                    })
                    .catch((error) => {
                      if (error.code === 'auth/email-already-in-use') {
                        console.log('That email address is already in use!');
                      }
                      if (error.code === 'auth/invalid-email') {
                        console.log('That email address is invalid!');
                      }
                      console.error(error);
                    });
                }}
              >
                <Text style={styles.footer}>Register</Text>
              </TouchableOpacity>

              <Text style={styles.footer}>{user ? user.email : 'None'}</Text>

            </View>
          </ViewWrapper>
        </ScrollWrapper>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 35,
    fontWeight: '800',
    color: '#FaFfF2',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default UserScreen;
