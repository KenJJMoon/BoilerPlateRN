/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { atom, useRecoilState } from 'recoil';
import styled from 'styled-components/native';

import { FilterSelect, HeaderStyle, Operations, Profile, SearchArea, ViewRowWrapper } from '../compos';
import { scrR } from '../compos/uiUtils';
import { filterState, likeState } from '../models';
import { designerWorks } from '../models/designerWorks';

declare const global: {HermesInternal: null | {}};

const iconDream = require('../images/icon_dream.png');
const iconSMS = require('../images/icon_sms.png');
const iconSearch = require('../images/search.png');

const viewHeight = scrR * 60;
const size46 = scrR * 46;
const size24 = scrR * 24;
const borderColor = '#292929';
const borderColorGray = '#29292933';

// Containers
const TopView = styled(ViewRowWrapper)`
  height: ${viewHeight}px;
  align-items: center;
`;

const TopMenu = styled(TopView)`
  height: 50px;
  border-color: ${borderColorGray};
  border-bottom-width: 1px;
  padding: 0px 20px 0px 20px;
`;

const MenuView = styled(ViewRowWrapper)
<{
  isActive?: boolean;
}>`
  height: 50px;
  align-items: center;
  justify-content: center;
  margin: 0px 30px 0px 0px;
  border-color: ${(p) => p.isActive ? borderColor : borderColorGray};
  border-bottom-width: ${(p) => p.isActive ? 1 : 0.5}px;
`;
const SearchView = styled(ViewRowWrapper)`
  align-items: center;
  width: 187px;
  height: 30px;
  border-radius: 5px;
  background: #f7f7fa;
`;

// Text

const TextMenu = styled(HeaderStyle)`
  color: #292929;
`;

// Image
const ImageIcon = styled.Image
<{
  size: number;
}>`
	width: ${(p) => p.size};
	height: ${(p) => p.size}; 
  margin: 0px 10px 0px 10px;
`;

const MainScreen = () => {
  const [filterStr, setFilterStr] = useRecoilState(filterState);
  const [likes, setLikes] = useRecoilState(likeState);
  console.log(' cur state ', filterStr);

  // if (!imgWorksDatum) return null;

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <TopView>
          <ImageIcon size={size46} source={iconDream} />
          <View style={{ flex: 1 }} />
          <ImageIcon size={size24} source={iconSMS} />
        </TopView>
        <TopMenu>
          <MenuView isActive >
            <TextMenu>디자이너</TextMenu>
          </MenuView>
          <MenuView >
            <TextMenu>스타일</TextMenu>
          </MenuView>
          <SearchView >
            <ImageIcon size={scrR * 20} source={iconSearch} />
          </SearchView>
        </TopMenu>
        <TopView>
          <FilterSelect title="지역" targetString="area" />
          <FilterSelect title="날짜" targetString="date"/>
          <FilterSelect title="시술/가격" targetString="price"/>
          <FilterSelect title="추천순" targetString="recommend"/>
        </TopView>

        {filterStr === 'area'
          ? <SearchArea />
          : <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            {designerWorks.map((dt) => {
              return (
                <Profile
                  key={dt.name}
                  data={dt}
                  rData={likes[dt.id]}
                />
              );
            },
            )}
          </ScrollView>
        }
        {filterStr !== 'area' &&
          (<View style={{
            position: 'absolute',
            left: 0,
            bottom: 220,
            height: 50,
            width: '100%',
            zIndex: 5,
            alignItems: 'center',
          }}>
            <Operations />
          </View>)
        }
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#fff',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default MainScreen;
