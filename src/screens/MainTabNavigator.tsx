import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { BackHandler, Platform } from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/AntDesign';
import IconEnt from 'react-native-vector-icons/Entypo';

import MainScreen from './MainScreen';
import SecondScreen from './SecondScreen';
import UserScreen from './UserScreen';

const BottomTab = createBottomTabNavigator();

const iconColor = (focused: boolean) : string => {
  return focused ? '#35F' : '#7669';
};

/**
 *
 * Company : statistics icon 필요 : line chard icon.
 */

const MainBottomTabNavigator = (): React.ReactElement => {
  const insets = useSafeArea();

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (Platform.OS === 'android') {
          console.log('TODO : handle back button');
        }
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, []),
  );

  return (
    <BottomTab.Navigator
      initialRouteName="MAIN"
      tabBarOptions={{
        showLabel: false,
        style: {
          height: 58 + insets.bottom,
        },
        labelStyle: {
          marginBottom: 8,
          fontSize: 10,
        },
      }}>
      <BottomTab.Screen
        name="MAIN"
        component={MainScreen}
        options={{
          tabBarIcon: (props: { focused: boolean; color: string; size: number }) => {
            const { focused } = props;
            return (
              <IconEnt
                // name="bars"
                name="menu"
                color={iconColor(focused)} size={30}
              />
            );
          },
        }}
      />

      <BottomTab.Screen
        name="SECOND"
        component={SecondScreen}
        options={{
          tabBarIcon: (props: { focused: boolean; color: string; size: number }) => {
            const { focused } = props;
            return (
              <IconEnt
                // name="message1"
                name="chat"
                color={iconColor(focused)} size={30}
              />
            );
          },
        }}
      />

      <BottomTab.Screen
        name="USER"
        component={UserScreen}
        options={{
          tabBarIcon: (props: { focused: boolean; color: string; size: number }) => {
            const { focused } = props;
            return (
              <IconEnt
                name="tools" // team when company case
                color={iconColor(focused)} size={30}
              />
              // <Icon
              //   name="user" // team when company case
              //   color={iconColor(focused)} size={30}
              // />
            );
          },
        }}
      />
    </BottomTab.Navigator>
  );
};

export default MainBottomTabNavigator;
