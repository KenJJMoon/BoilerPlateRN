/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  atom,
  RecoilRoot,
  selector,
  useRecoilState,
  useRecoilValue,
} from 'recoil';

import { AuthProvider, CompanyProvider } from './models';
import AppNavigator from './navigation/AppNavigator';

declare const global: {HermesInternal: null | {}};

function App(): React.ReactElement {
  return (
    <RecoilRoot>
      <CompanyProvider>
        <AuthProvider>
          <AppNavigator />
        </AuthProvider>
      </CompanyProvider>
    </RecoilRoot>
  );
}

export default App;
