module.exports = {
  root: true,
  extends: ['standard', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'simple-import-sort'],
  rules: {
    indent: ['error', 2, {
      outerIIFEBody: 0
    }],
    'no-unused-vars': 'off',
    'jsx-quotes': ['error', 'prefer-double'],
    // '@typescript-eslint/no-unused-vars': 'warn',
    '@typescript-eslint/interface-name-prefix': 0,
    'no-console': 'off',
    'react/jsx-uses-vars': [2],
    curly: ['error', 'multi-line'],
    'max-len': [
      'error',
      {
        code: 120,
        ignoreRegExpLiterals: true,
        ignoreComments: true,
        ignoreUrls: true,
      },
    ],
    'comma-dangle': ['error', 'always-multiline'],
    semi: [2, 'always'],
    'arrow-parens': ['error', 'always'],
    'no-new-object': 'error',
    'no-array-constructor': 'error',
    'sort-imports': 'off',
    'import/order': 'off',
    // 'sort-imports': [
    // 	2,
    // 	{
    // 		ignoreCase: false,
    // 		ignoreMemberSort: false,
    // 		memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
    // 	},
    // ],
    'object-curly-spacing': 1,
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always',
      },
    ],
    'react/prop-types': 0,
    'generator-star-spacing': ['error', {
      before: false,
      after: true
    }],
    '@typescript-eslint/strict-boolean-expressions': ['warn', {
      allowNullable: true
    }],
    'simple-import-sort/sort': 'error',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    // '@typescript-eslint/no-unused-var': 'off', 
    "no-tabs": 0,
    "no-mixed-spaces-and-tabs": 0,
    "react/display-name": 0,
    "@typescript-eslint/no-empty-function": 0,
  },
};
