//
//  HPBridge.h
//  HardPillarMobile
//
//  Created by KenMoon on 2020/08/05.
//

#ifndef HPBridge_h
#define HPBridge_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>

@interface HPBridge : RCTEventEmitter <RCTBridgeModule>

// -(void)makeSipCall:(NSString *_Nullable)phoneNumber;

@end

#endif /* HPBridge_h */
