//
//  HPBridge.m
//  HardPillarMobile
//
//  Created by KenMoon on 2020/08/05.
//


#import "HPBridge.h"

static NSString *const RNStateUpdate = @"RNStateUpdate";

@implementation HPBridge
{
  NSTimer *repetedTimer;
//  NSDictionary *inboundInfo;
//  BOOL isJsReady;
//  NSString *callNumberForJS;
}


RCT_EXPORT_MODULE()

- (instancetype)init {
  NSLog(@"RN HardPillar Bridge Init");
  self = [super init];
  repetedTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer){
    // UIApplicationStateActive, UIApplicationStateInactive, UIApplicationStateBackground
    
    NSLog(@"RN HardPillar Bridge : tick  %ld", [[UIApplication sharedApplication] applicationState]);
  }];
  
  
//  [[NSNotificationCenter defaultCenter] addObserver:self
//                                           selector:@selector(continueCallActivity:)
//                                               name:@"OutboundUserActivity"
//                                             object:nil];
//  [[NSNotificationCenter defaultCenter] addObserver:self
//                                           selector:@selector(cancelCallkitInbound:)
//                                               name:@"CallKitInboundCancel"
//                                             object:nil];
  return self;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

// Override method of RCTEventEmitter
- (NSArray<NSString *> *)supportedEvents
{
    return @[
        RNStateUpdate // [self sendEventWithName:RNStateUpdate body:@{@"number": phoneNumber}];
    ];
}

#pragma mark - Notification selectors

/**
 Make SIP call
 */
//-(void)makeSipCall:(NSString *)phoneNumber {
//  NSLog(@" RN Switch Bridge :: to %@", phoneNumber);
//  [self sendEventWithName:RNSwitchRequestSipCall body:@{@"number": phoneNumber}];
//}


#pragma mark - Export React Method

RCT_EXPORT_METHOD(startCall)
{
//  backgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
    // Completion handler to be performed if time runs out
//  }];
}

//RCT_EXPORT_METHOD(fetchVoIP:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
//{
//  NSUserDefaults *defaultObj = [NSUserDefaults standardUserDefaults];
//  NSString *uuid = [defaultObj stringForKey:@"VoIP_UUID"];
//  NSString *handle = [defaultObj stringForKey:@"VoIP_Handle"];
//  NSLog(@" RN Switch Bridge :: uuid %@, handle : %@", uuid, handle);
//  if (uuid == nil) {
//    NSError *error = [NSError errorWithDomain:@"SwitchBridge" code:99 userInfo:nil];
//    reject(@"no_voip_token", @"VoIP token is not received yet !!", error);
//    return;
//  }
//  NSMutableDictionary *voipDic = [[NSMutableDictionary alloc] init];
//  [voipDic setValue:uuid forKey:@"UUID"];
//  [voipDic setValue:handle forKey:@"handle"];
//  NSData* jsonData = [NSJSONSerialization dataWithJSONObject:voipDic options:NSJSONWritingPrettyPrinted error:nil];
//  NSString* jsonDataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//  NSLog(@" RN Switch Bridge :: json : %@", jsonDataStr);
//  resolve(jsonDataStr);
//}

@end
  

